# 2023 Aa Rg Monotonicity Maxwell

## License

Copyright (c) 2023, Annalena Albicker, Roland Griesmaier

This software is released under GNU General Public License, Version 3.
The full license text is provided in the file LICENSE included in this repository
or can be obtained from http://www.gnu.org/licenses/

## Content of this project

This is a guide to generate the figures and tables that have been used in the work

**"Monotonicity in inverse scattering for Maxwell's equations"**

by Annalena Albicker and Roland Griesmaier.

The following versions of the paper are available:

- [ ] [CRC 1173 Preprint 2021/29, October 2021](https://www.waves.kit.edu/downloads/CRC1173_Preprint_2021-29.pdf)
- [ ] [_Inverse Prob. Imaging_, **17**(1):68–105, 2023](https://www.aimsciences.org//article/doi/10.3934/ipi.2022032)

The code is in the Matlab programming language. You find all needed Matlab files to generate the figures.

## Requirements

The following additional software is necessary to run the code in this project:

- [ ] a recent version of Matlab
- [ ] the Matlab Parallelization Toolbox

## An overview

**Folders:**
- [ ] the produced figures are stored in the folder **plots**
- [ ] the folder **data** contains the far field matrices of the scattering objects in **Example 9.1** and **Example 9.2** as well as the error matrix used in **Example 9.1**
- [ ] the folder **reconstructions** contains the reconstruction matrices produced within the file *driver.m*

**Matlab files:**
- [ ] *blocks_exact.m* creates a plot of the exact shape of the mixed scattering object in **Example 9.2**
- [ ] *definite_par.m* shows the reconstruction in the x-y-plane given the far field operator ("definite test from inside")
- [ ] *definite3D_par.m* calculates the reconstruction matrix given the far field operator ("definite test from inside")
- [ ] *driver.m* contains different sections that produce the different figures in the paper
- [ ] *eigenvaluesTRFr.m* creates plots of the eigenvalues of the difference between the real part of the far field operator and the probing operator
- [ ] *eigTBexplicit_ball.m* calculates the eigenvalues of the probing operator when the underlying probing domain is a ball with center in the origin
- [ ] *evaluateTB_ball.m* evaluates the probing operator when the underlying probing domain is a ball
- [ ] *evalvsh.m* evaluates spherical harmonics on a sampling grid
- [ ] *GLNodeWt.m* calculates nodes and weights for Gauss-Legendre quadrature of arbitrary order
- [ ] *indefinite3D_par.m* calculates the reconstruction matrix given the far field operator ("indefinite test from outside")
- [ ] *newresclegendreprime.m* evaluates the derivative of a rescaled legendre function
- [ ] *prepareSamplingGrid.m* prepares the sampling grid on the sphere
- [ ] *torus_exact.m* creates a plot of the exact shape of the torus-shaped scattering object in **Example 9.1**
- [ ] *visualizeReconstruction.m* visualizes the three-dimensional reconstruction given the reconstruction matrix
- [ ] *vsht.m* implements the discrete vector spherical harmonics transform

## Generating the files

The file *driver.m* contains a section for each of the figures. Running the corresponding section generates the associated figures and stores them in the folder **plots**.
