function y = newresclegendreprime(n,x)
%% EVALUATES DERIVATIVE OF RESCALED LEGENDRE FUNCTION
% 
% INPUT: 
% n - order
% x - argument (allowed to be a vector)
%
% OUTPUT: 
% value of derivative of rescaled Legendre function of order n in x
%
% USED IN: evalvsh, vsht

mvec = 0:n;
nx = length(x);

Pmn = legendre(n,x)';
Pmnminus1 = [legendre(n-1,x)' zeros(nx,1)];

term1 = n*repmat(x,1,n+1).*Pmn;
term2 = (n+repmat(mvec,nx,1)).*Pmnminus1;

y = (- term1 + term2);

M = repmat(mvec,nx,1);
y = (-1).^M .* sqrt((n+1/2).*factorial(n-M)./factorial(n+M)) .* y;

end
