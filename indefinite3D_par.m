function Rec = indefinite3D_par(k, N, F, epsrel, alpha, hB, noiselevel, cores)
%% CALCULATE RECONSTRUCTION MATRIX GIVEN THE FAR FIELD OPERATOR (INDEFINITE TEST FROM OUTSIDE)
%
% INPUT:
% k - exterior wave number 
% N - maximal degree of vector spherical harmonic to use
% F - far field matrix
% epsrel - relative electric permittivity inside the scatterer
% alpha - parameter in the monotonicity test 
% hB - radius of the test ball
% noiselevel - relative noiselevel
% cores - number of cores available for parallel computingclear all

%% start parallel computing
folder = fileparts(which(mfilename));
addpath(genpath(folder));
parpool(cores)

%% parameters
kappa = sqrt(epsrel)*k; % interior wave number
q = 1-1/epsrel; % contrast
signContrast = sign(q);

thresholdEigenvalues = max([1e-14, 1*noiselevel]);  % don't use eigenvalues with absolute values smaller than this threshold

% sampling grid in region of interest
RGrid = 3;   % radius 
hGrid = RGrid/60;  % step size

fprintf('Exterior wave number: k = %2.2f\n', k)
fprintf('Wave length: lambda = %2.2f\n', 2*pi/k)
fprintf('Relative electric permittivity: epsrel = %2.2f\n', epsrel)
fprintf('Interior wave number: kappa = %2.2f\n', kappa)
fprintf('Contrast: q = %2.2f\n', q)
fprintf('Maximal degree of vector spherical harmonic used: N = %2.2f\n', N)

fprintf('Parameter in monotonicity test: alpha = %2.3f\n\n', alpha)

fprintf('Noise level: noiselevel = %1.2e\n\n', noiselevel)
fprintf('Threshold in monotonicity test: delta = %1.2e\n\n', thresholdEigenvalues)

%% farfield operator
eigF = eig(F);

% add noise to the data
if noiselevel > 0
    errmat = rand(size(F))-.5+1i*(rand(size(F))-.5);
    errmat = errmat ./ norm(errmat);
%    load('farfield matrices/errmat1e-3.mat');
    F = F + errmat * noiselevel * norm(F);
    clear errmat
end

EstNormalityError = norm(F*F'-F'*F);  % absoluter Datenfehler
fprintf('Estimated normality error of F: %1.2e\n', EstNormalityError)

% visualize eigenvalues of F
EigF = eig(F);

% check whether corresponding scattering operator is unitary (suggests that solver is working correctly)
Q = N*(N+2);
S = eye(2*Q) + (1i*k/(8*pi^2)).*F;  % scattering operator (should be unitary)

EstUnitarityError = norm(S*(S')-eye(2*Q));  % test relative unitarity error of scattering operator
fprintf('Estimated unitarity error of S: %1.2e\n', EstUnitarityError)

EigF(abs(EigF)<thresholdEigenvalues) = 0;
fprintf('\nNumber of negative eigenvalues of Re(F): %i\n', sum(real(EigF)<0));
fprintf('\nNumber of positive eigenvalues of Re(F): %i\n', sum(real(EigF)>0));

%% simple version of a monotonicity test
fprintf('\nSimple monotonicity test:\n')

ReF = (F+F')/2;

[XGrid,YGrid,ZGrid] = meshgrid(-RGrid:hGrid:RGrid,-RGrid:hGrid:RGrid,-RGrid:hGrid:RGrid);
nt = size(XGrid,1);
% initialize reconstruction matrix;
Rec = 1e6*ones(nt,nt,nt);

% prepare sampling grid
type = 'Gauss';
nphi = 12;  % number of discretization points in phi direction
mtheta = nphi/2; % number of discretization points in theta direction
sampling = prepareSamplingGrid('Gauss',nphi,mtheta);
nmax = 5; % cutoff index in eigenvalue decomposition

XGridVec = reshape(XGrid,1,nt*nt*nt);
YGridVec = reshape(YGrid,1,nt*nt*nt);
ZGridVec = reshape(ZGrid,1,nt*nt*nt);

zBVec = [XGridVec;YGridVec;ZGridVec];

% count number of positive eigenvalues of the test operator for each grid
% point
parfor kk = 1 : nt*nt*nt
        zB = zBVec(:,kk);
        TB = evaluateTB_ball(k,zB,hB,N,sampling,nmax);
        A = -signContrast.*(ReF-alpha.*TB);
        EigA = eig(A);
        NuOfUsedEigenvaluesVec(kk)=sum(abs(EigA)>=thresholdEigenvalues);
        EigA(abs(EigA)<thresholdEigenvalues) = 0;
        sumEigAVec(kk) = sum(real(EigA)<0);
end
NuOfUsedEigenvalues = reshape(NuOfUsedEigenvaluesVec,nt,nt,nt);
sumEigA = reshape(sumEigAVec,nt,nt,nt);

% for each grid point calculate minimal number of positive eigenvalues that
% was achieved with a ball containing this grid point
for iterx=1:nt
    for itery=1:nt
        for iterz=1:nt
            zB = [XGrid(iterx,itery,iterz),YGrid(iterx,itery,iterz),ZGrid(iterx,itery,iterz)];  % center of B
            index = ((XGrid-zB(1)).^2+(YGrid-zB(2)).^2+(ZGrid-zB(3)).^2<hB.^2);
            Rec(index) = min(Rec(index), sumEigA(iterx,itery,iterz));
        end
    end
end

fprintf('Number of eigenvalues used (on average): %2.1f\n\n', mean(mean(NuOfUsedEigenvalues)))

%% end parallel computing
poolobj = gcp('nocreate');
delete(poolobj);

end