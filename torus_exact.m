function torus_exact
%% CREATE PLOT OF THE EXACT GEOMETRY OF THE TORUS

figure()
R=1; % outer radius of torus
r=0.2; % inner tube radius
th=linspace(0,2*pi,128); % partitions along perimeter of the tube 
phi=linspace(0,2*pi,128); % partitions along azimuth of torus
% we convert our vectors phi and th to [n x n] matrices
[Phi,Th]=meshgrid(phi,th); 
% generate [n x n] matrices for x,y,z according to equation of torus
x=(R+r.*cos(Th)).*cos(Phi);
y=(R+r.*cos(Th)).*sin(Phi);
z=r.*sin(Th);

surf(x,y,z); % plot surface
set(gca,'YDir','normal');
shading flat;
xlim([-3, 3]);
ylim([-3, 3]);
zlim([-3, 3]);
axis square
set(gca,'Fontsize',28)
map = [0 0 1
    0 0 0.9
    0 0 0.8];
colormap(map) % change color appearance
hold on 
% create shadows
sx = size(x);
sy = size(y);
sz = size(z);
hright = plot3(3 * ones(sx),y,z,'o','MarkerSize',.5,'Color',[0.5 0.5 0.5]);
hleft = plot3(x,3 * ones(sx),z,'o','MarkerSize',.5,'Color',[0.5 0.5 0.5]);
hdown = plot3(x,y,-3 * ones(sy),'o','MarkerSize',.5,'Color',[0.5 0.5 0.5]);
axis equal
axis([-3,3,-3,3,-3,3])
grid on
set(gca,'Xtick',[-3:1:3]);
set(gca,'Ytick',[-3:1:3]);
set(gca,'Ztick',[-3:1:3]);
camlight; lighting 'flat',
ax = gca;
ax.CameraPosition = [-30.04984382404462,-33.86501532180962,25.49838472147062];
ax.CameraViewAngle = 10.3396;
ax.Box = 'on';

set(findall(gcf,'type','axes'),'fontsize',10)
set(findall(gcf,'type','text'),'fontSize',20) 

end