function visualizeReconstruction(Rec, iso, dir, val)
%% VISUALIZE 3D RECONSTRUCTION GIVEN THE RECONSTRUCTION MATRIX
%  
% INPUT:
% Rec - reconstruction matrix
% iso - truncation index for isosurface
% z - z-value for volume slice

RGrid = 3;   % radius 
hGrid = RGrid/60;  % step size
[XGrid,YGrid,ZGrid] = meshgrid(-RGrid:hGrid:RGrid,-RGrid:hGrid:RGrid,-RGrid:hGrid:RGrid);

figure()
TruncLevel = iso;
LvlI = Rec.*(Rec<=TruncLevel);

LvlIVec = logical(LvlI(:));

XVec = XGrid(:);
YVec = YGrid(:);
ZVec = ZGrid(:);
IVec = Rec(:);

NewIVec = IVec;
NewIVec(~LvlIVec) = 0;
NewI = reshape(NewIVec,121,121,121);
iso = isosurface(XGrid,YGrid,ZGrid, NewI,0);
% plot isosurface
p = patch(iso);

xlim([-3, 3]);
ylim([-3, 3]);
zlim([-3, 3]);
isonormals(XGrid,YGrid,ZGrid, Rec, p)
p.FaceColor = 'blue';
p.EdgeColor = 'none';

% create shadows
hold on 
hright = plot3(3 * ones(1,sum(LvlIVec)),YVec(LvlIVec),ZVec(LvlIVec),'o','MarkerSize',.5);
hleft = plot3(XVec(LvlIVec),3 * ones(1,sum(LvlIVec)),ZVec(LvlIVec),'o','MarkerSize',.5);
hdown = plot3(XVec(LvlIVec),YVec(LvlIVec),-3 * ones(1,sum(LvlIVec)),'o','MarkerSize',.5);
hdown.LineWidth = 1;
hdown.MarkerSize = 1;
hdown.Marker = 'o'; %Marker becomes a sphere
hdown.MarkerEdgeColor = [0.5 0.5 0.5]; %Edge color is black
hdown.MarkerFaceColor = [0.5 0.5 0.5];  %Face color is black
hleft.LineWidth = 1;
hleft.Marker = 'o'; %Marker becomes a sphere
hleft.MarkerEdgeColor = [0.5 0.5 0.5]; %Edge color is black
hleft.MarkerFaceColor = [0.5 0.5 0.5];  %Face color is black
hright.LineWidth = 1;
hright.Marker = 'o'; %Marker becomes a sphere
hright.MarkerEdgeColor = [0.5 0.5 0.5]; %Edge color is black
hright.MarkerFaceColor = [0.5 0.5 0.5];  %Face color is black
axis equal
axis([-3,3,-3,3,-3,3])
grid on
set(gca,'Xtick',[-3:1:3]);
set(gca,'Ytick',[-3:1:3]);
set(gca,'Ztick',[-3:1:3]);
camlight; lighting 'flat';
ax = gca;
ax.CameraPosition = [-30.04984382404462,-33.86501532180962,25.49838472147062];
ax.CameraViewAngle = 10.3396;
ax.Box = 'on';

hold on
if dir == 'x'
    im1 = slice(XGrid, YGrid, ZGrid, Rec, val, [], []);
elseif dir == 'y'
    im1 = slice(XGrid, YGrid, ZGrid, Rec, [], val, []);
elseif dir == 'z'
    im1 = slice(XGrid, YGrid, ZGrid, Rec, [], [], val);
end

im1.FaceColor = 'flat';
im1.FaceLighting = 'none';
im1.FaceAlpha = 0.5;
im1.EdgeColor = 'none';

c = colorbar('FontSize',12);
set(c,'Position',[0.85 0.2 0.03 0.6])
cbmin = c.Limits(1);
cbmax = c.Limits(2);
set(gca, 'clim', c.Limits);
set(c, 'ticks', cbmin:cbmax);
drawnow;
alphaVal = 0.5;
% Make the colorbar transparent
cdata = c.Face.Texture.CData;
cdata(end,:) = uint8(alphaVal * cdata(end,:));
c.Face.Texture.ColorType = 'truecoloralpha';
c.Face.Texture.CData = cdata;
drawnow
% Make sure that the renderer doesn't revert your changes
c.Face.ColorBinding = 'discrete';

set(findall(gcf,'type','axes'),'fontsize',10)
set(findall(gcf,'type','text'),'fontSize',20) 
end