function [au,av] = vsht(f,sampling,nmax)
%% DISCRETE VECTOR SPHERICAL HARMONICS TRANSFORM
%
% INPUT: 
% f - to be transformed 
% sampling - sampling grid on unit sphere
% nmax - cutoff index in eigenvalue decomposition
% 
% OUTPUT:
% au - discrete vector spherical transform with U_a^b 
%                (a=1,...,nmax, b=-a,...a)
% av - discrete vector spherical transform with V_a^b 
%                (a=1,...,nmax, b=-a,...a)
%
% USED IN: evaluateTB_ball

theta = sampling.theta; % sampling grid in polar coordinates
phi = sampling.phi;
mtheta = sampling.mtheta;
nphi = sampling.nphi;
w = sampling.wtheta;  % quadrature weights


%% COMPUTE VSH-COEFFICIENTS USING FFT AND QUADRATURE RULE
thetahat = zeros(mtheta+1,nphi,3);
thetahat(:,:,1) = cos(theta)*cos(phi);
thetahat(:,:,2) = cos(theta)*sin(phi);
thetahat(:,:,3) = repmat(-sin(theta),1,nphi);

phihat = zeros(mtheta+1,nphi,3);
phihat(:,:,1) = repmat(-sin(phi),mtheta+1,1);
phihat(:,:,2) = repmat(cos(phi),mtheta+1,1);

au = zeros((nmax+1)^2-1,1);  % initialize variable for the SH-coefficients
av = zeros((nmax+1)^2-1,1);

for n = 1:nmax

    fft_fthetahat_help = 2*pi/nphi * fft(sum(f.*thetahat,3),[],2);  % evalate phi-integral using fft
    fft_fthetahat = [fft_fthetahat_help(:,end-n+1:end) fft_fthetahat_help(:,1:n+1)];  % extract relevant m-components for the current nu
    
    fft_fphihat_help = 2*pi/nphi * fft(sum(f.*phihat,3),[],2);  % evalate phi-integral using fft
    fft_fphihat = [fft_fphihat_help(:,end-n+1:end) fft_fphihat_help(:,1:n+1)];  % extract relevant m-components for the current nu
    
%     CmnPmnhelp = (1i/sqrt(2*pi)) .* resclegendre(n,cos(theta));
    CmnPmnhelp = (1i*repmat(0:n,mtheta+1,1)./sqrt(2*pi)) .* (legendre(n,cos(theta),'norm')');  % evaluate properly rescaled Legendre polynomials
    CmnPmn = [-CmnPmnhelp(:,end:-1:2) CmnPmnhelp(:,1:end)];
    
%     CmnSinthetaPmnprimehelp = 1/sqrt(2*pi) .* resclegendreprime(n,cos(theta));
    CmnSinthetaPmnprimehelp = 1/sqrt(2*pi) * newresclegendreprime(n,cos(theta));
%     CmnSinthetaPmnprimehelp = 1/sqrt(2*pi) * repmat(sin(theta),1,n+1) .* resclegendreprime(n,cos(theta));
    CmnSinthetaPmnprime = [CmnSinthetaPmnprimehelp(:,end:-1:2) CmnSinthetaPmnprimehelp(:,1:end)];
    
    aun_help = 1/sqrt(n*(n+1)) * (-CmnSinthetaPmnprime.*fft_fthetahat - CmnPmn.*fft_fphihat);
    avn_help = 1/sqrt(n*(n+1)) * (-CmnSinthetaPmnprime.*fft_fphihat + CmnPmn.*fft_fthetahat);
    
    aun = w' * aun_help;  % quadrature over theta
    avn = w' * avn_help;
    
    nhelp = n^2;
    au(nhelp:nhelp+2*n) = aun.';
    av(nhelp:nhelp+2*n) = avn.';
    
end

end
