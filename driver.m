%% figure 9.1
eigenvaluesTRFr(1, 2, 5, 0.5, 0, 25, 1000);
% print('fig91','-depsc');

%% figure 9.2
% plot of the exact torus (left)
torus_exact;
% print('fig92left','-depsc');

% 3D reconstruction from exact data (middle)
load('data/torus_farfieldoperator.mat'); % load farfield matrix
% calculate "reconstruction matrix", i.e. number of negative eigenvalues in
% each grid point
cores = 32; % number of cores available for parallel computing
Rec1 = definite3D_par(1, 5, F, 2, 20, 0, cores);
% visualize reconstruction
visualizeReconstruction(Rec1, 2, 'z', 0);
% print('fig92middle','-depsc');

% 3D reconstruction using noisy data (right)
load('data/torus_farfieldoperator.mat'); % load farfield matrix
% calculate "reconstruction matrix", i.e. number of negative eigenvalues in
% each grid point
% these plots are created with random noise - in the folder data you find
% the error matrices used in the paper
cores = 32; % number of cores available for parallel computing
Rec2 = definite3D_par(1, 5, F, 2, 20, 1e-3, cores);
% visualize reconstruction
visualizeReconstruction(Rec2, 11, 'z', 0);
% print('fig92right','-depsc');

%% figure 9.3
% reconstruction from exact data for different alpha in the x-y-plane
load('data/torus_farfieldoperator.mat'); % load farfield matrix
alpha = [0.01; 0.1; 0.5; 1; 10; 20];
cores = 32; % number of cores available for parallel computing
for j=1:6
definite_par(1, 5, F, alpha(j), 0, cores);
% print(['fig93_',num2str(j),'.eps'],'-depsc');
end

%% figure 9.4
% plot of the exact blocks (left)
blocks_exact;
print('fig94left','-depsc');

% 3D reconstruction of the lower block with strictly positive contrast
% (middle)
load('data/blocks_farfieldoperator.mat'); % load farfield matrix
% calculate "reconstruction matrices", i.e. minimal number of positive 
% eigenvalues in each grid point
cores = 32; % number of cores available for parallel computing
Rec3 = indefinite3D_par(1, 5, F, 2, 0.5, 0.75, 0, cores);
visualizeReconstruction(Rec3,7,'z',-0.5);
% print('fig94middle','-depsc');

% 3D reconstruction of the upper block with strictly negative contrast
% (right)
load('data/blocks_farfieldoperator.mat'); % load farfield matrix
% calculate "reconstruction matrices", i.e. minimal number of positive 
% eigenvalues in each grid point
cores = 32; % number of cores available for parallel computing
Rec4 = indefinite3D_par(1, 5, F, 0.5, -1, 0.75, 0, cores);
visualizeReconstruction(Rec4,1,'z',1.25);
% print('fig94right','-depsc');