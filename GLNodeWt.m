function [x,w] = GLNodeWt(n)
%% NODES AND WEIGHTS FOR GAUSS-LEGENDRE QUADRRATURE 
% of arbitrary order obtained by solving an eigenvalue problem
%
% INPUT:
% n = order of quadrature rule
%
% OUTPUT:    
% x = vector of nodes
% w = vector of weights


beta   = (1:n-1)./sqrt(4*(1:n-1).^2 - 1);
J      = diag(beta,-1) + diag(beta,1);    % eig(J) needs J in full storage
[V,D]  = eig(J);
[x,ix] = sort(diag(D));  %  nodes are eigenvalues, which are on diagonal of D
w      = 2*V(1,ix)'.^2;  %  V(1,ix)' is column vector of first row of sorted V 
