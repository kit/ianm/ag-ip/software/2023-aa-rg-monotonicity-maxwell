function eigenvaluesTRFr(k, epsr, r, alpha, threshold, Rmax, Nmax)
%%  SHOW PLOT OF EIGENVALUES OF TR-Fr
%
% INPUT:
% k - exterior wave number 
% epsr - relative electric permittivity inside the scatterer
% r - radius of scatterer
% alpha - parameter in the monotonicity test 
% threshold - discard eiganvalues smaller than threshold
% Rmax - maximal radius of test ball
% Nmax - number of eigenvalues that are computed

%% PARAMETERS
lambda = 2*pi/k;  % wave length

kappa = sqrt(epsr)*k;  q = 1-1/epsr;  % material parameter inside scatterer

nvec = 1:Nmax; 

fprintf('\nWave length: %2.1f\n\n', lambda)
fprintf('\nRadius of scatterer: %2.1f\n\n', r)


%% SPHERICAL BESSEL FUNCTIONS AND DERIVATIVES
sphericalbesselj = @(n,z) sqrt(pi/(2*z)) .* besselj(n+1/2,z);
sphericalbesseljprime = @(n,z) -sphericalbesselj(n+1,z) + n/z .* sphericalbesselj(n,z);

sphericalhankel = @(n,z) sqrt(pi/(2*z)) .* besselh(n+1/2,z);
sphericalhankelprime = @(n,z) -sphericalhankel(n+1,z) + n/z .* sphericalhankel(n,z);


%% EIGENVALUES OF Trho
sn2 = @(n,rho) 2*pi*(rho)^3*( sphericalbesselj(n,rho).^2 - sphericalbesselj(n-1,rho).*sphericalbesselj(n+1,rho) );

rn2 = @(n,rho) sn2(n,rho) + 2*pi*rho^2*( 2*sphericalbesselj(n,rho).*sphericalbesseljprime(n,rho) ) ...
        + 4*pi*rho*( sphericalbesselj(n,rho).^2 );

eigMuSpheroidalOfTBrho = @(n,rho) 4*pi/k * sn2(n,k*rho);
eigMuToroidalOfTBrho = @(n,rho) 4*pi/k * rn2(n,k*rho);


%% EIGENVALUES OF Frho
zaehler_lambdaS = @(n,rho) (kappa*rho)*sphericalbesselj(n,k*rho).*sphericalbesseljprime(n,kappa*rho) ...
    - (k*rho)*sphericalbesselj(n,kappa*rho).*sphericalbesseljprime(n,k*rho);

nenner_lambdaS = @(n,rho) (k*rho)*sphericalbesselj(n,kappa*rho).*sphericalhankelprime(n,k*rho) ...
    - (kappa*rho)*sphericalhankel(n,k*rho).*sphericalbesseljprime(n,kappa*rho);

eigLambdaSpheroidalOfFBrho = @(n,rho) -1i*(4*pi)^2/k .* zaehler_lambdaS(n,rho) ./ nenner_lambdaS(n,rho);


zaehler_lambdaT = @(n,rho) -q*sphericalbesselj(n,k*rho).*sphericalbesselj(n,kappa*rho) ...
    + (kappa*rho/epsr)*sphericalbesselj(n,k*rho).*sphericalbesseljprime(n,kappa*rho) ...
    - (k*rho)*sphericalbesselj(n,kappa*rho).*sphericalbesseljprime(n,k*rho);

nenner_lambdaT = @(n,rho) q*sphericalhankel(n,k*rho).*sphericalbesselj(n,kappa*rho) ...
    + (k*rho)*sphericalhankelprime(n,k*rho).*sphericalbesselj(n,kappa*rho) ...
    - (kappa*rho)/epsr*sphericalbesseljprime(n,kappa*rho).*sphericalhankel(n,k*rho);

eigLambdaToroidalOfFBrho = @(n,rho) -1i*(4*pi)^2/k .* zaehler_lambdaT(n,rho) ./ nenner_lambdaT(n,rho);

eigFBr = [eigLambdaToroidalOfFBrho(nvec,r), eigLambdaSpheroidalOfFBrho(nvec,r)];


%%
Rvec = 0.01*lambda:0.01*lambda:Rmax;
lengthRvec = length(Rvec);
qvecFBrminusalphaTBRneg = zeros(lengthRvec,1); qvecFBrminusalphaTBRminuspos = zeros(lengthRvec,1);
qvecTBRpos = zeros(lengthRvec,1); qvecTBRneg = zeros(lengthRvec,1);
for iter = 1:lengthRvec
    
    eigTBR = [eigMuToroidalOfTBrho(nvec,Rvec(iter)), eigMuSpheroidalOfTBrho(nvec,Rvec(iter))];
    
    eigFBrminusalphaTBR = eigFBr-alpha*eigTBR;
    
    qvecFBrminusalphaTBRminuspos(iter) = sum(real(eigFBrminusalphaTBR)>threshold);
    qvecFBrminusalphaTBRneg(iter) = sum(real(eigFBrminusalphaTBR)<-threshold);
    
    qvecTBRpos(iter) = sum(real(alpha*eigTBR)>threshold);
    qvecTBRneg(iter) = sum(real(alpha*eigTBR)<-threshold);

end

qvecFBrpos = sum(real(eigFBr)>threshold) * ones(lengthRvec,1);
qvecFBrneg = sum(real(eigFBr)<-threshold) * ones(lengthRvec,1);


%%  PLOTS
figure()
plot(Rvec,qvecFBrminusalphaTBRneg,'k-','linewidth',2)
hold on
plot(Rvec,qvecFBrneg,'r:','linewidth',2)
plot(Rvec,qvecTBRpos,'b--','linewidth',2)
hold off
legend('#\{Re(\lambda_n(kr_D))-\mu_n(kr_B))\}<-\delta','#\{Re(\lambda_n(kr_D))\}<-\delta','#\{\mu_n(kr_B)\}>\delta','Location','NorthWest')
set(gca,'Fontsize',16)
xlabel('r_B')
title(['$k=',num2str(k),', r_D=',num2str(r),', \alpha=',num2str(alpha),', \delta=',num2str(threshold),'$'],'Interpreter','latex')
set(findall(gcf,'type','axes'),'fontsize',12)
set(findall(gcf,'type','text'),'fontSize',12) 
print('plots/fig1_left','-depsc');

figure()
plot(Rvec,qvecFBrminusalphaTBRminuspos,'k-','linewidth',2)
hold on
plot(Rvec,qvecFBrpos,'r:','linewidth',2)
plot(Rvec,qvecTBRneg,'b--','linewidth',2)
hold off
legend('#\{Re(\lambda_n(kr_D))-\mu_n(kr_B))\}>\delta','#\{Re(\lambda_n(kr_D))\}>\delta','#\{\mu_n(kr_B)\}<-\delta','Location','NorthEast')
set(gca,'Fontsize',16)
xlabel('r_B')
title(['$k=',num2str(k),', r_D=',num2str(r),', \alpha=',num2str(alpha),', \delta=',num2str(threshold),'$'],'Interpreter','latex')
set(findall(gcf,'type','axes'),'fontsize',12)
set(findall(gcf,'type','text'),'fontSize',12) 
print('plots/fig1_right','-depsc');

end
