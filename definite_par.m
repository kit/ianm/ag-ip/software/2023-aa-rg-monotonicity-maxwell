function definite_par(k, N, F, alpha, noiselevel,cores)
%% SHOW RECONSTRUCTION IN THE X-Y-PLANE GIVEN THE FAR FIELD OPERATOR (DEFINITE TEST FROM INSIDE)
% 
% INPUT:
% k - exterior wave number 
% N - maximal degree of vector spherical harmonic to use
% F - far field matrix
% geometry - geometry of the scatterer (1 = torus, 2 = quader)
% EpsRel - relative electric permittivity inside the scatterer
% alpha - parameter in the monotonicity test 
% noiselevel - relative noiselevel
% cores - number of cores available for parallel computing

%% start parallel computing
folder = fileparts(which(mfilename));
addpath(genpath(folder));
parpool(cores)

%% parameters
thresholdEigenvalues = max([1e-14, 1*noiselevel]);  % don't use eigenvalues with absolute values smaller than this threshold

% sampling grid in region of interest
RGrid = 3;   % radius 
hGrid = RGrid/60;  % step size

fprintf('\nParameters:\n')
fprintf('Wave number: k = %2.2f\n', k)
fprintf('Wave length: lambda = %2.2f\n', 2*pi/k)
fprintf('Maximal degree of vector spherical harmonic used: N = %2.2f\n', N)

fprintf('Parameter in monotonicity test: alpha = %2.3f\n\n', alpha)

fprintf('Noise level: noiselevel = %1.2e\n\n', noiselevel)
fprintf('Threshold in monotonicity test: delta = %1.2e\n\n', thresholdEigenvalues)

%% farfield operator
EigF = eig(F);
EpsRel = 2; 
kappa = sqrt(EpsRel)*k; % interior wave number
q = 1-1/EpsRel; % contrast

fprintf('Contrast: q = %2.2f\n', q)

% add noise to the data
if noiselevel > 0
    errmat = rand(size(F))-.5+1i*(rand(size(F))-.5);
    errmat = errmat ./ norm(errmat);
    % in order to use the noise matrix that we used in the paper
%    load('data/errmat1e-3.mat'); 
    F = F + errmat * noiselevel * norm(F);
    clear errmat
end

EstNormalityError = norm(F*F'-F'*F);  % absoluter Datenfehler
fprintf('Estimated normality error of F: %1.2e\n', EstNormalityError)

% check whether corresponding scattering operator is unitary (suggests that solver is working correctly)
Q = N*(N+2);
S = eye(2*Q) + (1i*k/(8*pi^2)).*F;  % scattering operator (should be unitary)

EstUnitarityError = norm(S*(S')-eye(2*Q));  % test relative unitarity error of scattering operator
fprintf('Estimated unitarity error of S: %1.2e\n', EstUnitarityError)

EigF(abs(EigF)<thresholdEigenvalues) = 0;
fprintf('\nNumber of negative eigenvalues of Re(F): %i\n', sum(real(EigF)<0));
fprintf('\nNumber of positive eigenvalues of Re(F): %i\n', sum(real(EigF)>0));

%% simple version of a monotonicity test
fprintf('\nSimple monotonicity test:\n')

signContrast = sign(q);

ReF = (F+F')/2;

[XGrid,YGrid] = meshgrid(-RGrid:hGrid:RGrid,-RGrid:hGrid:RGrid);

nt = size(XGrid,1);

% prepare sampling grid
type = 'Gauss';
nphi = 12;  % number of discretization points in phi direction
mtheta = nphi/2; % number of discretization points in theta direction
sampling = prepareSamplingGrid('Gauss',nphi,mtheta);
nmax = 5; % cutoff index in eigenvalue decomposition

zB3 = 0; % third component of test ball 

XGridVec = reshape(XGrid,1,nt*nt);
YGridVec = reshape(YGrid,1,nt*nt);
zBVec = [XGridVec;YGridVec;zB3.*ones(1,nt*nt)];
hB = hGrid;  % side length of B = pixel centered at zB

parfor kk = 1 : nt*nt
        zB = zBVec(:,kk);
        TB = evaluateTB_ball(k,zB,hB,N,sampling,nmax);
        A = signContrast.*(ReF-alpha.*TB);
        EigA = eig(A);
        NuOfUsedEigenvaluesVec(kk)=sum(abs(EigA)>=thresholdEigenvalues);
        EigA(abs(EigA)<thresholdEigenvalues) = 0;
        IVec(kk) = sum(real(EigA)<0);
end
NuOfUsedEigenvalues = reshape(NuOfUsedEigenvaluesVec,nt,nt);
I = reshape(IVec,nt,nt);

fprintf('Number of eigenvalues used (on average): %2.1f\n\n', mean(mean(NuOfUsedEigenvalues)))

%  show reconstruction
figure()
im = imagesc([-RGrid,RGrid],[-RGrid, RGrid],I);

set(gca,'YDir','normal');
shading flat;
xlim([-RGrid RGrid]); ylim([-RGrid RGrid]);
xlim([-3 3]); ylim([-3 3]);
axis square
colorbar
set(gca,'Fontsize',28)
title(['$k=',num2str(k),', \alpha=',num2str(alpha),', N=',num2str(N),'$'],'Interpreter','latex');
cb = colorbar;
cbmin = cb.Limits(1);
cbmax = cb.Limits(2);
set(gca, 'clim', cb.Limits);
set(cb, 'ticks', cbmin:cbmax); 

phip = linspace(0,2*pi);
snp = sin(phip); csp = cos(phip);
ccircle = 0;
rcircle1 = 0.8;
circle1 = ccircle + rcircle1*(csp+1i*snp);
rcircle2 = 1.2;
circle2 = ccircle + rcircle2*(csp+1i*snp);
hold on
plot(real(circle1),imag(circle1),'--', 'color', [0, 1, 0],'linewidth',2)
plot(real(circle2),imag(circle2),'--', 'color', [0, 1, 0],'linewidth',2)
hold off

set(findall(gcf,'type','axes'),'fontsize',15)
set(findall(gcf,'type','text'),'fontSize',15) 

%% end parallel computing
poolobj = gcp('nocreate');
delete(poolobj);

end

