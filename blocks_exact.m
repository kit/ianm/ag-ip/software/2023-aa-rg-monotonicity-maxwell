function blocks_exact
%% CREATE PLOTS OF THE EXACT GEOMETRY OF THE BLOCKS

figure()
% lower block
[X Y] = meshgrid(linspace(-1,-0.5,40),linspace(-1,-0.5,40));
Z1 = -1*ones(length(X),length(Y));
Z2 = 0*ones(length(X),length(Y));
% plot surfaces of the block
surf(X,Y,Z1);
hold on;
% create shadows
hdown = plot3(X,Y,-3 * ones(size(Z1)),'o','MarkerSize',.5,'Color',[0.5 0.5 0.5]);
surf(X,Y,Z2);
[X Z] = meshgrid(linspace(-1,-0.5,40),linspace(-1,0,40));
Y1 = -1*ones(length(X),length(Z));
Y2 = -0.5*ones(length(X),length(Z));
hleft = plot3(X,3 * ones(size(Y1)),Z,'o','MarkerSize',.5,'Color',[0.5 0.5 0.5]);
surf(X,Y1,Z);
surf(X,Y2,Z);
[Y Z] = meshgrid(linspace(-1,-0.5,40),linspace(-1,0,40));
X1 = -1*ones(length(Y),length(Z));
X2 = -0.5*ones(length(Y),length(Z));
hright = plot3(3 * ones(size(X1)),Y,Z,'o','MarkerSize',.5,'Color',[0.5 0.5 0.5]);
surf(X1,Y,Z);
surf(X2,Y,Z);

% upper block
[X Y] = meshgrid(linspace(1,2,40),linspace(1,1.5,40));
Z1 = 1*ones(length(X),length(Y));
Z2 = 1.5*ones(length(X),length(Y));
% plot surfaces of the block
surf(X,Y,Z1);
hold on;
% create shadows
hdown = plot3(X,Y,-3 * ones(size(Z1)),'o','MarkerSize',.5,'Color',[0.5 0.5 0.5]);
surf(X,Y,Z2);
[X Z] = meshgrid(linspace(1,2,40),linspace(1,1.5,40));
Y1 = 1*ones(length(X),length(Z));
Y2 = 1.5*ones(length(X),length(Z));
hleft = plot3(X,3 * ones(size(Y1)),Z,'o','MarkerSize',.5,'Color',[0.5 0.5 0.5]);
surf(X,Y1,Z);
surf(X,Y2,Z);
[Y Z] = meshgrid(linspace(1,1.5,40),linspace(1,1.5,40));
X1 = 1*ones(length(Y),length(Z));
X2 = 2*ones(length(Y),length(Z));
hright = plot3(3 * ones(size(X1)),Y,Z,'o','MarkerSize',.5,'Color',[0.5 0.5 0.5]);
surf(X1,Y,Z);
surf(X2,Y,Z);

shading flat;
map = [0 0 1
    0 0 0.9
    0 0 0.8];
colormap(map); % change color appearance
axis equal
xlim([-3, 3]);
ylim([-3, 3]);
zlim([-3, 3]);
axis([-3,3,-3,3,-3,3])
grid on
% grid minor

set(gca,'Xtick',[-3:1:3]);
set(gca,'Ytick',[-3:1:3]);
set(gca,'Ztick',[-3:1:3]);

camlight; lighting 'flat';
ax = gca;
ax.CameraPosition = [-30.04984382404462,-33.86501532180962,25.49838472147062];
ax.CameraViewAngle = 10.3396;
ax.Box = 'on';

set(findall(gcf,'type','axes'),'fontsize',10)
set(findall(gcf,'type','text'),'fontSize',20) 

end