function [sampling] = prepareSamplingGrid(type,nphi,mtheta)
%% PREPARES SAMPLING GRID ON THE SPHERE
%
% INPUT:   
% type - 'Gauss' or 'equiangular'
% nphi - number of discretization points in phi direction
% mtheta - number of discretization points in theta direction
%
% OUTPUT:  
% sampling (struct) includes
%           sampling.type
%           sampling.nphi
%           sampling.mtheta
%           
%           sampling.phi
%           sampling.theta
%           sampling.wtheta (quadrature weights)
%           sampling.xhat (grid points on sphere)

sampling.type = type;
sampling.nphi = nphi;
sampling.mtheta = mtheta;
switch sampling.type
    
    case 'equiangular'
        
        theta = (0:mtheta)'/mtheta*pi;
        phi = (0:nphi-1)/nphi*2*pi;
        
        % quadrature weights (see Driscoll & Healy, p. 217)
        PInhelp = zeros(mtheta+1,mtheta/2);
        for iterl = 1:mtheta/2
            PInhelp(:,iterl) = sin((2*iterl-1)*theta) / (2*iterl-1);
        end
        Pin = (4/pi) * sum(PInhelp, 2);
        w = pi/mtheta * Pin;
%         w = w .* sin(theta);
        
    case 'Gauss'
        
        [x_theta,w] = GLNodeWt(mtheta+1);
        theta = flipud(acos(x_theta));
        phi = (0:nphi-1)/nphi*2*pi;
        
        clear x_theta
        
        w = w./sin(theta);
        
end
sampling.theta = theta;
sampling.phi = phi;
sampling.wtheta = w;

xhat = zeros(mtheta+1,nphi,3);
xhat(:,:,1) = sin(theta)*cos(phi);
xhat(:,:,2) = sin(theta)*sin(phi);
xhat(:,:,3) = repmat(cos(theta),1,nphi);
sampling.xhat = xhat;