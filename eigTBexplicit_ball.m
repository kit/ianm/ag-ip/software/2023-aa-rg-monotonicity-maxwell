function [eig] = eigTBexplicit_ball(k,R,N,mult)
%% CALCULATES THE EIGENVALUES OF THE OPERATOR TB=H_B^*H_B OF THE MONOTONICITY TEST
%
% INPUT:   
% k - wave number
% R - radius of test ball B (centered in the origin)
% N - maximal degree of vector spherical harmonic to use
% mult - 'yes': with multiplicities, 'no': without multiplicities
%
% OUTPUT:  
% eig - eigenvalues of the test operator
%
% USED IN: evaluateTB_ball

%% SPHERICAL BESSEL FUNCTIONS AND DERIVATIVES
spherBessel = @(n,z) sqrt(pi/(2*z)) .* besselj(n+1/2,z);
spherBesselder = @(n,z) -spherBessel(n+1,z) + n/z .* spherBessel(n,z);

%%
switch mult
    case 'yes'
        n = zeros(1,N*(N+2));
        index = 1;
        for i = 1:N
            m = 2*i;
            n(index:index+m) = i;
        index = index+m+1;
        end
    case 'no'
        n = 1:N;
end

sn = @(n,r) 2*pi*r^3*( spherBessel(n,r).^2 - spherBessel(n-1,r).*spherBessel(n+1,r) );

lambdas = 4*pi/k*sn(n,k*R);

lambdat = 4*pi/k*(sn(n,k*R) + 2*pi*R^2*( 2*spherBessel(n,k*R).*spherBesselder(n,k*R) ) ...
        + 4*pi*R*( spherBessel(n,k*R).^2 ));
    
eig = [lambdas, lambdat];
