function [Umn,Vmn] = evalvsh(sampling,n,m)
%% EVALUATE SPHERICAL HARMONICS ON GRID
%
% INPUT:    
% sampling - sampling grid on unit sphere
% n, m - orders
%
% OUTPUT:   
% Umn, Vmn - vector spherical harmonics evaluated on sampling grid
%
% USED IN:  evaluateTB_ball

%  grid parameters
phi = sampling.phi;
theta = sampling.theta;
nphi = sampling.nphi;
mtheta = sampling.mtheta;

%%
thetahat = zeros(mtheta+1,nphi,3);
thetahat(:,:,1) = cos(theta)*cos(phi);
thetahat(:,:,2) = cos(theta)*sin(phi);
thetahat(:,:,3) = repmat(-sin(theta),1,nphi);

phihat = zeros(mtheta+1,nphi,3);
phihat(:,:,1) = repmat(-sin(phi),mtheta+1,1);
phihat(:,:,2) = repmat(cos(phi),mtheta+1,1);

Umn = zeros(mtheta+1,nphi,3);
Vmn = zeros(mtheta+1,nphi,3);

%     CmnPmnhelp = (1i/sqrt(2*pi)) .* resclegendre(n,cos(theta));
CmnPmnhelp = (1i*repmat(0:n,mtheta+1,1)./sqrt(2*pi)) .* (legendre(n,cos(theta),'norm')') ./ repmat(sin(theta),1,n+1);  % evaluate properly rescaled Legendre polynomials
CmnPmn = [-CmnPmnhelp(:,end:-1:2) CmnPmnhelp(:,1:end)];

%     CmnSinthetaPmnprimehelp = 1/sqrt(2*pi) .* resclegendreprime(n,cos(theta));
CmnSinthetaPmnprimehelp = 1/sqrt(2*pi) * newresclegendreprime(n,cos(theta)) ./ repmat(sin(theta),1,n+1);
%     CmnSinthetaPmnprimehelp = 1/sqrt(2*pi) * repmat(sin(theta),1,n+1) .* resclegendreprime(n,cos(theta));
CmnSinthetaPmnprime = [CmnSinthetaPmnprimehelp(:,end:-1:2) CmnSinthetaPmnprimehelp(:,1:end)];

ehelp = exp(1i*m*phi);

Umn_help1 = 1/sqrt(n*(n+1)) * CmnSinthetaPmnprime(:,m+n+1)*ehelp;
Umn_help2 = 1/sqrt(n*(n+1)) * CmnPmn(:,m+n+1)*ehelp;

Vmn_help1 = 1/sqrt(n*(n+1)) * CmnSinthetaPmnprime(:,m+n+1)*ehelp;
Vmn_help2 = 1/sqrt(n*(n+1)) * CmnPmn(:,m+n+1)*ehelp;

for iter=1:3
    Umn(:,:,iter) = - Umn_help1.*thetahat(:,:,iter) + Umn_help2.*phihat(:,:,iter);
    Vmn(:,:,iter) = - Vmn_help1.*phihat(:,:,iter) - Vmn_help2.*thetahat(:,:,iter);
end


end

