function [TB] = evaluateTB_ball(k,z,R,N,sampling,nmax)
%% EVALUATES THE OPERATOR TB=H_B^*H_B OF THE MONOTONICITY TEST
%
% INPUT:   
% k - wave number
% z, R - center and radius of test ball B
% N - maximal degree of vector spherical harmonic to use
% sampling - sampling grid on unit sphere
% nmax - cutoff index in eigenvalue decomposition (nmax <= nphi/2)
%
% OUTPUT:  
% TB - operator TB (2*N*(N+2) times 2*N*(N+2))
%
% EXAMPLE USAGE: evaluateTB_ball(1,[1,0,0],2,10,sampling,10)

Q = N*(N+2);
TB = zeros(2*Q,2*Q);

xhat = sampling.xhat; % sampling points
mtheta = sampling.mtheta;
nphi = sampling.nphi;
scpxz = zeros(mtheta+1,nphi,3);
scpxz(:,:,1) = z(1).*xhat(:,:,1);
scpxz(:,:,2) = z(2).*xhat(:,:,2);
scpxz(:,:,3) = z(3).*xhat(:,:,3);
scpxz = sum(scpxz,3); % scalar product of xhat and z
expterm = exp(1i*k.*scpxz);

% eigenvalues of test operator centered in the origin
Qmax = nmax*(nmax+2);
lambda = eigTBexplicit_ball(k,R,nmax,'yes'); 
lambdas = lambda(1:Qmax)';
lambdat = lambda(Qmax+1:end)';

scalUU = zeros(Qmax,Q);
scalUV = zeros(Qmax,Q);
scalVU = zeros(Qmax,Q);
scalVV = zeros(Qmax,Q);

indexMax = N * (N+2);
for nn=1:N
    NVec(nn^2:nn^2+2*nn)=nn;
    MVec(nn^2:nn^2+2*nn)=-nn:1:nn;
end

for kk = 1:indexMax
        n = NVec(kk);
        m = MVec(kk);
        index = kk;
        [Umn,Vmn] = evalvsh(sampling,n,m);
        scalUmn = expterm.*Umn;
        scalVmn = expterm.*Vmn;
        [scalUU(:,index),scalUV(:,index)] = vsht(scalUmn,sampling,nmax);
        [scalVU(:,index),scalVV(:,index)] = vsht(scalVmn,sampling,nmax);
        
end
LambdasMat = repmat(lambdas,1,Q);
LambdatMat = repmat(lambdat,1,Q);


TB(1:N*(N+2),1:N*(N+2)) = ((LambdasMat.' .* scalUU.')*conj(scalUU) + (LambdatMat.' .* scalUV.')*conj(scalUV)).';
TB(1:N*(N+2),N*(N+2)+1:2*N*(N+2)) = ((LambdasMat.' .* scalVU.')*conj(scalUU) + (LambdatMat.' .* scalVV.')*conj(scalUV)).';
TB(N*(N+2)+1:2*N*(N+2),1:N*(N+2)) = ((LambdasMat.' .* scalUU.')*conj(scalVU) + (LambdatMat.' .* scalUV.')*conj(scalVV)).';
TB(N*(N+2)+1:2*N*(N+2),N*(N+2)+1:2*N*(N+2)) = ((LambdasMat.' .* scalVU.')*conj(scalVU) + (LambdatMat.' .* scalVV.')*conj(scalVV)).';
